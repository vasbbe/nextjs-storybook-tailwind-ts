# Setup Storybook Tailwind Typescript Next.js

[Original article](https://news.shardlabs.io/storybook-tailwind-next-js-with-typescript-5a2486f905ec/)

## Install

```shell

# Next.js
# -------
npx create-next-app your-project-name --use-npm

cd your-project-name

# TypeScript
# ----------
npm install --save-dev typescript @types/react @types/node
touch tsconfig.json
# the next command populates tsconfig.json file
npm run dev

# Prettier
# --------
npm install --save-dev prettier
touch .prettierrc

# Tailwind
# --------
npm install --save-dev tailwindcss
npx tailwindcss init
npm install --save-dev postcss postcss-flexbugs-fixes postcss-preset-env
touch postcss.config.js


# Storybook
# ---------
npx sb init
```

## `tailwind.config.js`

```css
module.exports = {
  content: ['./components/**/*.{tsx,jsx,js,ts}', './pages/**/*.{tsx,jsx,js,ts}'],
  theme: {
    extend: {}
  },
  plugins: []
};
```

## `postcss.config.js`

```css
module.exports = {
  plugins: {
    tailwindcss: {
    }
    ,
    'postcss-flexbugs-fixes': {
    }
    ,
    'postcss-preset-env': {
      autoprefixer: {
        flexbox: 'no-2009';
      }
      ,
      stage: 3,
      features: {
        'custom-properties':false ;
      }
    }
  }
}
```

## `global.css`

```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```

## `.prettierrc`

```js
{
  "semi": true,
  "tabWidth": 2,
  "printWidth": 100,
  "singleQuote": true,
  "trailingComma": "none",
  "jsxBracketSameLine": true
}
```

## `.storybook/main.js`

change default location of stories to `src/components`

```js
module.exports = {
  stories: ['../components/**/*.stories.mdx', '../components/**/*.stories.@(js|jsx|ts|tsx)']
  //...
};
```

## `.storybook/preview.js`

enable tailwind in Storybook

```js
import '../styles/globals.css';
```

`npm run storybook` will fail until stories are created.

```shell
mkdir components
```
