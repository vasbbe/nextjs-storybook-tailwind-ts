import { classesSize, classesVariant } from './Button.style';

import React from 'react';

interface Props {
  children: React.ReactNode;
  onClick: () => void;
  variant?: 'default' | 'primary' | 'info' | 'success' | 'warning' | 'danger' | 'dark';
  size?: keyof typeof classesSize;
  disabled?: boolean;
}

export const Button: React.FC<Props> = ({
  children,
  disabled,
  onClick,
  size = 'medium',
  variant = 'default',
  ...rest
}) => {
  const buttonSize = classesSize?.[size] || classesSize.md;
  const buttonVariant = classesVariant?.[variant] || classesVariant.default;
  const isDisabled = disabled ? 'disabled' : '';
  return (
    <button
      className={`btn ${buttonVariant} ${buttonSize} ${isDisabled}`}
      onClick={onClick}
      disabled={disabled}
      {...rest}>
      {children}
    </button>
  );
};

export default Button;
