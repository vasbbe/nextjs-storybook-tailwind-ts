export const classesSize = {
  small: 'px-4 py-2 rounded-sm border-2 text-sm',
  medium: 'px-6 py-3 rounded-md border-2', //24px 12px
  default: 'px-6 py-3 rounded-md'
};

export const classesVariant = {
  default:
    'bg-white-100 text-black hover:bg-black hover:text-white font-semibold leading-4 border-current',
  primary: 'bg-black text-white  font-semibold leading-4',
  info: 'bg-blue-500 text-white',
  success: 'bg-green-500 text-white',
  warning: 'bg-yellow-400 text-white',
  danger: 'bg-red-500 text-white',
  dark: 'bg-gray-800 text-white'
};
