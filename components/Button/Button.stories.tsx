import { Button, ButtonProps } from './Button';
import { Meta, Story } from '@storybook/react';

import React from 'react';

export default {
  title: 'Atoms/Button',
  component: Button
  // argTypes: {
  //   backgroundColor: { control: 'color' },
  //   textColor: { control: 'color' },
  //   variant:{control: ''}
  // }
} as Meta;

export const Default: Story<ButtonProps> = (args) => <Button {...args} />;
Default.args = {
  children: 'Default',
  variant: 'default button'
};

export const SmallDefalult: Story<ButtonProps> = (args) => <Button {...args} />;
SmallDefalult.args = {
  children: 'Small Defalult button',
  variant: 'default',
  size: 'small'
};

export const Primary: Story<ButtonProps> = (args) => <Button {...args} />;
Primary.args = {
  children: 'Primary button',
  variant: 'primary'
};

export const Small: Story<ButtonProps> = (args) => <Button {...args} />;
Small.args = {
  children: 'Small button',
  variant: 'primary',
  size: 'small'
};

export const Medium: Story<ButtonProps> = (args) => <Button {...args} />;
Medium.args = {
  children: 'Medium button',
  variant: 'primary',
  size: 'medium'
};
