module.exports = {
  content: ['./components/**/*.{tsx,jsx,js,ts}', './pages/**/*.{tsx,jsx,js,ts}'],
  theme: {
    extend: {
      colors: {
        'bg-black': '#020201'
      }
    }
  },
  plugins: []
};
